%{ 
Programa que permite analizar el comportamiento de un sistema de LC (Respuesta al escalón, LdR, Bode), por medio de las herramientas "rlocus" y "sisotool". 

. R(s): entrada escalón unitario (1/s)  
. C(s): controlador PID  
. G(s): planta 
. H(s): realimentación unitaria negativa.  

. Kc: ganancia crítica obtenida por Routh.
. Wc: frecuencia crítica correspondiente a Kc
. Pc: período de oscilación crítico

Parámetros del PID: para calcularlos utilizamos el "Método de Oscilación de Z-N" (método 2)

. Kp: K proporcional
. Ti: T integral
. Td: T derivativo
. Ki: K integral
. Kd: K derivativo

%}

% Variable compleja de Laplace
s=tf('s');

% Realimentación
H=1;
% Planta 
G=100/((s+8)*(s+7.5)*(s+4.5));

% Valores críticos
Kc=23.25;
Wc=11.39;
Pc=(2*pi)/Wc;

% Parámetros PID
Kp=0.6*Kc;
Ti=Pc/2;
Td=Pc/8;
Ki=Kp/Ti;
Kd=Kp*Td;

% Controlador PID
C=Kp+Ki/s+Kd*s;
%C=(51.66*(s+6.66)*(s+8.33))/s

% Análisis de LC sin compensar
%rl=rlocus(G*H)
%st=sisotool(G*H);

% Análisis de LC compensado
%rl=rlocus(C*G*H)
st=sisotool(C*G*H);

