# Trabajo entregable PID

## IC 2023

### Estructura del repositorio

* `main.tex`: es el archivo principal de latex, en el cual se define la estructura, se importan las librerias y las distintas secciones del documento
* `content`: en este directorio se encuentran los distintos archivos que componen el cuerpo del documento
* `images`: se encuentran las imágenes del documento.
* `src`: se encuentran los códigos y archivos de simulacion que utilizamos para el análisis del trabajo:
  * `tf.ipynb`: código de python en formato "Jupiter Notebook" y la librería **[Control Systems](https://python-control.readthedocs.io/en/0.9.4/)** que permite realizar el análisis del sistema.
  * `tf.m`: código de matlab que permite realizar el análisis del sistema.
  * `simutacion.slx`: simulacion en simulink (de matlab) de los sistemas de la, lc compensado y sin compensar 
